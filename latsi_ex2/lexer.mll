{
open Parser
}

let layout = [ ' ' '\t'  '\r']
let var = ['A'-'Z']
let chiffre = ['0'-'9']
let nombre = chiffre+
let relop = '<'('>'|'=')? | '>'('<'|'=')?
let string = '\"'[' ' '0'-'9' ',' '\'' '_' ';' ':' '=' '(' ')' '.'  'a'-'z' 'A'-'Z']*'\"'

rule main = parse
  | layout		      { main lexbuf }
  | '\n'            { CR }
  | "IMPRIME"       { IMPRIME }
  | "SI"            { SI }
  | "ALORS"         { ALORS }
  | "VAVERS"        { VAVERS }
  | "ENTREE"        { ENTREE }
  | "FIN"           { FIN }
  | "REM"           { REM }
  | "NL"            { NL }
  | '='             { EGAL }
  | ','             { VIR }
  | '+'             { PLUS }
  | '-'             { MOINS }
  | '*'             { MULT }
  | '/'             { DIV }
  | '('			        { LPAREN }
  | ')'			        { RPAREN }
  | '>'             { SUP }
  | ">="            { SUP_EGAL }
  | '<'             { INF }
  | "<="            { INF_EGAL }
  | ("<>"|"><")     { DIFFERENT }
  | nombre          { NB (int_of_string(Lexing.lexeme lexbuf)) }
  | var             { VAR (Lexing.lexeme lexbuf) }
  | string          { STRING (Lexing.lexeme lexbuf) }
  | eof			        { EOF }
  | _			          { print_endline (Lexing.lexeme lexbuf); failwith "unexpected character" }
