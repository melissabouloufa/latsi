open Ast

let lexbuf = Lexing.from_channel (open_in Sys.argv.(1)) 

let remove_guillemets s = 
  String.sub s 1 ((String.length s)-2)


let rec eval_expression env e = 
  match e with
  | PlusE (t, e') -> (eval_term env t) + (eval_expression env e')
  | MoinsE (t, e') -> -(eval_term env t) + (eval_expression env e')
  | Plus t -> eval_term env t
  | Moins t -> -(eval_term env t)
and eval_term env t = 
  match t with 
  | Mult (f, t) -> (eval_facteur env f) * (eval_term env t) 
  | Div (f, t) -> (eval_facteur env f) / (eval_term env t) 
  | F f -> eval_facteur env f
and eval_facteur env f = 
  match f with 
  | Var x -> 
    begin 
      try List.assoc x env with
      | Not_found -> failwith ("Unbound variable")
    end
  | Nb n -> n
  | E ex -> eval_expression env ex

let rec print_expr_list env el = 
  match el with 
  | [] -> ()
  | p::el' -> 
      match p with 
      | String s -> (print_string (remove_guillemets s));(print_expr_list env el')
      | Expression e -> (print_int (eval_expression env e));
                        (print_expr_list env el')

let eval_relop r n1 n2 = 
  match r with 
  | Inf         -> n1 < n2
  | Inf_ou_egal -> n1 <= n2
  | Egal        -> n1 = n2
  | Different   -> n1 <> n2
  | Sup_ou_egal -> n1 >= n2
  | Sup         -> n1 > n2

let rec eval_entree env l = 
  match l with
  | [] -> env
  | a::l' -> 
        print_endline("Valeur de " ^ a ^ " :");
        match read_int_opt() with 
        | Some i -> 
          if List.exists (fun x -> (fst x)=a ) env then 
            eval_entree (List.map (fun (x,v) -> if x = a then (a,i) else (x,v)) env) l'
          else
            eval_entree ((a, i)::env) l'
        | None -> print_endline("Veuillez entre un entier.");
                  eval_entree env l

let rec eval env l programme =
  match l with 
  | [] -> ()
  | a::l' -> let res = (eval_ligne env a programme) in
              match res with
              | Some e -> eval e l' programme
              | None -> () 

and eval_ligne env a programme  = 
  eval_instr env (snd a) programme

and eval_instr env i programme = 
  match i with 
  | Imprime l -> print_expr_list env l; Some env
  | Si (e1, r, e2, i') -> if (eval_relop r (eval_expression env e1) (eval_expression env e2)) 
                          then (eval_instr env i' programme) 
                          else Some env
  | Vavers e -> eval_vavers env (eval_expression env e) programme programme
  | Entree l -> Some (eval_entree env l)
  | Definition (lv, le) -> 
    (*on parcourt les deux listes simultanément pour associer
       chaque variable à sa valeur*)
      let rec list_def l1 l2 acc =
        begin match l1 with 
        | [] -> begin match l2 with
                | [] -> Some acc
                (*les deux listes doivent avoir la même taille...*)
                | _ -> print_endline("ERREUR DECLARATION DE VARIABLES : il manque la valeur d'une variable"); Some acc
                end
        | c::l1' -> begin match l2 with
                    | [] -> print_endline("ERREUR DECLARATION DE VARIABLES : il manque un nom de variable"); Some acc
                    | e::l2' ->
                      (*s'il existe déjà la variable c dans notre environnement
                         on va modifier sa valeur*)
                      if List.exists (fun x -> (fst x)=c ) env then 
                        list_def l1' l2' (List.map (fun (x,v) -> if x = c then (c, eval_expression acc e) else (x,v)) acc)
                      (*sinon on l'initialise*)
                      else
                        list_def l1' l2' ((c, eval_expression env e)::acc)
                      end
        end
      in list_def lv le env
  | Fin -> None 
  | Rem  -> Some env
  | Nl -> print_newline (); Some env

and eval_vavers env n l programme = 
  match l with 
  | [] -> print_endline("ERREUR VAVERS : ligne "^(string_of_int n)^" non trouvée"); Some env
  | a::l' -> if (fst a)=n then (eval env l programme; None)
              else eval_vavers env n l' programme

let eval_first env l programme =
  match l with 
    | [] -> failwith "programme vide"
    | _ -> eval env l programme

let tri l =
  List.sort (fun (n,_) (n1,_) -> if n>n1 then 1 else if n<n1 then -1 else 0 ) l 

let rec remove_doublon l res= 
  match l with 
  | [] -> res
  | a::l -> if List.exists (fun x -> (fst x)= (fst a) ) res then remove_doublon l res
            else remove_doublon l (a::res)

let ordonner_programme l = 
  tri (remove_doublon (List.rev l) [])
           
let ast = Parser.input Lexer.main lexbuf 

(*let _ = Printf.printf "Parse:\n%s\n" (Ast.as_string ast)*)
let programme = ordonner_programme ast

let _ = (eval_first [] programme programme)