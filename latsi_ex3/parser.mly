%{
open Ast
%}

%token EOF CR IMPRIME SI ALORS VAVERS ENTREE FIN REM NL SOUSROUTINE RETOURNE
%token EGAL VIR PLUS MOINS MULT DIV LPAREN RPAREN
%token SUP INF SUP_EGAL INF_EGAL DIFFERENT 
%token<int> NB
%token<string> STRING VAR

%start<Ast.ligne list> input
%%

input: l= list(ligne) EOF { l }

ligne: 
n=NB i=instr list(CR) { (n,i) }

instr: 
IMPRIME l=expr_list { Imprime (l) }
| SI e1=expression r=relop e2=expression ALORS i1=instr {Si (e1, r, e2, i1)}
| VAVERS e=expression { Vavers (e) }
| SOUSROUTINE e=expression { SousRoutine (e) }
| RETOURNE {Retourne}
| ENTREE l=var_list { Entree (l) }
| v=VAR EGAL e=expression { Definition (v,e) }
| FIN { Fin }
| REM STRING { Rem }
| NL { Nl }

relop: 
SUP         { Sup } 
| INF       { Inf } 
| SUP_EGAL  { Sup_ou_egal } 
| INF_EGAL  { Inf_ou_egal } 
| EGAL      { Egal } 
| DIFFERENT { Different }


expr_list: 
| l=separated_nonempty_list(VIR, printable) { l }

printable:
| s=STRING { String (s) }
| e=expression { Expression (e) }

var_list: 
l=separated_nonempty_list(VIR, VAR) { l }

expression:
| PLUS? t1=term t2=term_signe? { 
    match t2 with 
    | Some a -> PlusE (t1, a) 
    | None -> Plus (t1) }
| MOINS t1=term t2=term_signe? { 
    match t2 with 
    | Some a -> MoinsE (t1, a) 
    | None -> Moins (t1)} 

term: 
| f=facteur { F f }
| f=facteur MULT t=term { Mult (f, t) }
| f=facteur DIV t=term { Div (f, t) }

term_signe: 
| PLUS t=term e=term_signe? { 
    match e with 
    | Some a -> PlusE (t, a) 
    | None -> Plus (t)
}
| MOINS t=term e=term_signe? { 
    match e with 
    | Some a -> MoinsE (t, a) 
    | None -> Moins (t)
}

facteur: 
| v=VAR { Var (v) } 
| n=NB  { Nb (n) } 
| LPAREN e=expression RPAREN { E (e) }