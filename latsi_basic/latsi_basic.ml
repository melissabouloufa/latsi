open Ast

let lexbuf = Lexing.from_channel (open_in Sys.argv.(1)) 

(*fonction qui enleve les guillemets de s*)
let remove_guillemets (s : string) : (string) = 
  String.sub s 1 ((String.length s)-2)

(*evalue l'expression e en se basant sur l'environnement *)
let rec eval_expression (env : (string * 'a) list) (e : expression) : (int)= 
  match e with
  | PlusE (t, e') -> (eval_term env t) + (eval_expression env e')
  | MoinsE (t, e') -> -(eval_term env t) + (eval_expression env e')
  | Plus t -> eval_term env t
  | Moins t -> -(eval_term env t)
(*evalue un terme en se basant sur l'environnement*)
and eval_term (env : (string * 'a) list) (t : term) : (int)= 
  match t with 
  | Mult (f, t) -> (eval_facteur env f) * (eval_term env t) 
  | Div (f, t) -> (eval_facteur env f) / (eval_term env t) 
  | F f -> eval_facteur env f
and eval_facteur (env : (string * 'a) list) (f : facteur) : (int) = 
  match f with 
  | Var x -> 
    begin 
      try List.assoc x env with
      | Not_found -> failwith ("Unbound variable")
    end
  | Nb n -> n
  | E ex -> eval_expression env ex

let rec print_expr_list (env : (string* 'a) list) (el : printable list) : (unit) = 
  match el with 
  | [] -> ()
  | p::el' -> 
      match p with 
      | String s -> (print_string (remove_guillemets s));(print_expr_list env el')
      | Expression e -> (print_int (eval_expression env e));
                        (print_expr_list env el')

let eval_relop (r : relop) (n1:int) (n2:int) : (bool) = 
  match r with 
  | Inf         -> n1 < n2
  | Inf_ou_egal -> n1 <= n2
  | Egal        -> n1 = n2
  | Different   -> n1 <> n2
  | Sup_ou_egal -> n1 >= n2
  | Sup         -> n1 > n2

let rec eval_entree (env : (string* 'a) list) (l : string list) : ((string * 'a) list) = 
  match l with
  | [] -> env
  | a::l' -> 
        print_endline("Valeur de " ^ a ^ " :");
        match read_int_opt() with 
        | Some i -> 
          if List.exists (fun x -> (fst x)=a ) env then 
            eval_entree (List.map (fun (x,v) -> if x = a then (a,i) else (x,v)) env) l'
          else
            eval_entree ((a, i)::env) l'
        | None -> print_endline("Veuillez entre un entier.");
                  eval_entree env l
(*évalue chaque ligne du programme*)
(*programme est une copie de la liste de ligne originelle 
   ce qui nous permet de nous en souvenir pour les instructions comme VAVERS*)
let rec eval (env : (string * 'a) list) (l : (int*instr) list) (programme : (int*instr) list) : (unit) =
  match l with 
  | [] -> ()
  | a::l' -> let res = (eval_ligne env a programme) in
              match res with
              | Some e -> eval e l' programme
              | None -> () (*si l'évaluation renvoie none c'est que le programme a fini*)

and eval_ligne (env : (string * 'a) list) (a:int*instr) (programme : (int*instr) list) : ((string * 'a) list option)  = 
  eval_instr env (snd a) programme

and eval_instr (env : (string * 'a) list) (i: instr) (programme : (int*instr) list) : ((string * 'a) list option)= 
  match i with 
  | Imprime l -> print_expr_list env l; Some env
  | Si (e1, r, e2, i') -> if (eval_relop r (eval_expression env e1) (eval_expression env e2)) 
                          then (eval_instr env i' programme) 
                          else Some env
  | Vavers e -> eval_vavers env (eval_expression env e) programme programme
  | Entree l -> Some (eval_entree env l)
  | Definition (c, e) -> 
      (*si la variable existe déjà on modifie sa valeur*)
      if List.exists (fun x -> (fst x)=c ) env then 
        Some (List.map (fun (x,v) -> if x = c then (c, eval_expression env e) else (x,v)) env)
      else
        (*sinon on la crée (initialise)*)
        Some ((c, eval_expression env e)::env)
  | Fin -> None 
  | Rem  -> Some env
  | Nl -> print_newline (); Some env

and eval_vavers (env : (string * 'a) list) (n:int) (l : (int*instr) list) (programme : (int*instr) list) : ((string * 'a) list option)= 
  match l with 
  | [] -> print_endline("ERREUR VAVERS : ligne "^(string_of_int n)^" non trouvée"); Some env
  | a::l' -> if (fst a)=n then (eval env l programme; None)
              else eval_vavers env n l' programme

(*verifie que le programme n'est pas vide avant de lancer l'évaluation*)
let eval_first (env : (string * 'a) list) (l : (int*instr) list) (programme : (int*instr) list) =
  match l with 
    | [] -> failwith "programme vide"
    | _ -> eval env l programme

let tri l =
  List.sort (fun (n,_) (n1,_) -> if n>n1 then 1 else if n<n1 then -1 else 0 ) l 

(* prend en argument une liste à l'envers pour ne garder que les dernieres lignes du meme numéro*)
let rec remove_doublon (l : (int*instr) list) (res : (int*instr) list) : ((int*instr) list)= 
  match l with 
  | [] -> res
  | a::l -> if List.exists (fun x -> (fst x)= (fst a) ) res then remove_doublon l res
            else remove_doublon l (a::res)

let ordonner_programme (l: (int*instr) list) = 
  tri (remove_doublon (List.rev l) [])
           
let ast = Parser.input Lexer.main lexbuf 

let programme = ordonner_programme ast

let _ = (eval_first [] programme programme)