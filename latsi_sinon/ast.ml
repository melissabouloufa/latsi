
type expression = 
  | PlusE of term * expression
  | MoinsE of term * expression
  | Plus of term
  | Moins of term
and facteur = 
  | Var of string
  | Nb of int
  | E of expression
and term = 
  | Mult of facteur * term
  | Div of facteur * term
  | F of facteur 

type printable = 
  | String of string
  | Expression of expression
  
type relop = 
  | Inf
  | Inf_ou_egal
  | Egal
  | Different
  | Sup_ou_egal
  | Sup 

type instr = 
  | Imprime of printable list
  | Si of expression * relop * expression * instr
  | Si_Sinon of expression * relop * expression * instr * instr
  | Vavers of expression
  | Entree of string list
  | Definition of string * expression
  | Fin
  | Rem 
  | Nl 

type ligne = int * instr